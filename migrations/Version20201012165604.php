<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201012165604 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('INSERT INTO `user` VALUES (NULL, "joker", "[\"ROLE_SUPER_ADMIN\"]", "$argon2id$v=19$m=65536,t=4,p=1$q0kQVBPwubYwbV2XuiK2CA$JniJX/z8AjSYkTcoWGLKAfkiigDpVUWkmMnKcbuq96o")');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DELETE FROM `user` WHERE username = "joker"');
    }
}
